import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if(! (liste.isEmpty())){
            Integer nombre = liste.get(0);
            for(Integer i = 1; i < liste.size(); i++){
                if(liste.get(i) < nombre){
                    nombre = liste.get(i);
                }
            }
            return nombre;
        }
        else{
            return null;
        }
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for (T obj : liste){
            if(valeur.compareTo(obj)>=0){
                return false ;
            }
        }
        return true;
    }




    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> listeFinale = new ArrayList<>();
        for(T elem : liste1){
            if(liste2.contains(elem)){
                listeFinale.add(elem);
            }
        }
        return listeFinale;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> listeFinale = new ArrayList<>();
        String[] liste = texte.split(" ");
        for (int x=0; x<liste.length; x++)
            if(liste[x].contains("a") || liste[x].contains("b") || liste[x].contains("c") || liste[x].contains("d") || liste[x].contains("e") || liste[x].contains("f") || liste[x].contains("g") || liste[x].contains("h") || liste[x].contains("i") || liste[x].contains("j") || liste[x].contains("k") || liste[x].contains("l") || liste[x].contains("m") || liste[x].contains("n") || liste[x].contains("o") || liste[x].contains("p") || liste[x].contains("q") || liste[x].contains("r") || liste[x].contains("s") || liste[x].contains("t") || liste[x].contains("u") || liste[x].contains("v") || liste[x].contains("w") || liste[x].contains("x") || liste[x].contains("y") || liste[x].contains("z")){
                listeFinale.add(liste[x]);
        }
        return listeFinale;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if (texte.equals("")){
            return null;
        }
    	List<String> mots = BibDM.decoupe(texte);
        int nbMot=0;
        int nbMotMax = 0;
        String motPrec = "";
        String motMax = "";
        Collections.sort(mots);
        for(String mot : mots){
            if (mot.equals(motPrec)){
                nbMot +=1;
            }
            else{
                if (nbMot>nbMotMax){
                    nbMotMax = nbMot;
                    motMax = motPrec;
                }
                nbMot = 1;
            }
            motPrec = mot ;
        }
        return motMax;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int nbOuvert = 0;
        int nbFerme = 0;
        for(char lettre : chaine.toCharArray()){
            if (lettre =='('){
                nbOuvert +=1;
            }
            if(lettre == ')'){
                nbFerme +=1;
            }
            if(nbOuvert<nbFerme){
                return false;
            }
        }
        return nbOuvert==nbFerme;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        if(BibDM.bienParenthesee(chaine)){
            int cpt1 = 0;
            int cpt2 = 0;
            String[] liste = chaine.split(" ");
            for (int x=0; x<liste.length; x++){
                for(int i=0; i<liste[x].length();i++){
                    cpt1 = 0;
                    cpt2 = 0;
                    if(liste[x].charAt(i) == '['){
                        cpt1++;
                    }
                    if(liste[x].charAt(i) == ']'){
                        cpt2++;
                    }
                    }
                }
            return cpt1 == cpt2;
            }
        else{
            return false;
        }

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        int a = 0;
        int b = liste.size();
        boolean res = false;
                while( a-b > 1 && !res){
                    if(valeur > liste.get(b/2)){
                        b = (b/2 + b/4);
                    }
                    else if(valeur > liste.get(b/2)){
                        b = (b/2);
                    }
                    else{
                        return true;
                    }	
            }
        return res;
    }
}
